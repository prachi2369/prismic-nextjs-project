import Button from "@/components/Button";
import { Content } from "@prismicio/client";
import { PrismicNextImage, PrismicNextLink } from "@prismicio/next";
import {
  JSXMapSerializer,
  PrismicRichText,
  SliceComponentProps,
} from "@prismicio/react";

const components: JSXMapSerializer = {
  heading1: ({ children }) => <h1 className="text-4xl text-center">{children}</h1>,
  paragraph: ({ children }) => <p className="text-2xl text-center">{children}</p>,
};
/**
 * Props for `Hero`.
 */
export type HeroProps = SliceComponentProps<Content.HeroSlice>;

/**
 * Component for "Hero" Slices.
 */
const Hero = ({ slice }: HeroProps): JSX.Element => {
  return (
    <>
      {slice.variation === "default" && (
        <section
          className="px-4 py-10 md:py-14 md:px-6 lg:py-16 flex justify-center flex-col items-center"
          data-slice-type={slice.slice_type}
          data-slice-variation={slice.variation}
        >
          <div className="mx-ato w-full max-w-6xl flex gap-4 flex-col">
            <PrismicRichText
              field={slice.primary.heading}
              components={components}
            />
            <PrismicRichText
              field={slice.primary.body}
              components={components}
            />

            <Button
              field={slice.primary.button_link}
              className="block w-fit bg-cyan-700 hover:bg-cyan-800 px-12 text-white rounded-full m-auto"
            >
              {slice.primary.button_text}
            </Button>
            <PrismicNextImage
              field={slice.primary.image}
              className="drop-shadow-xl max-w-4xl w-full m-auto"
            />
          </div>
        </section>
      )}
      {slice.variation === "horizontal" && (
        <section
          className="px-4 py-10 md:py-14 md:px-6 lg:py-16"
          data-slice-type={slice.slice_type}
          data-slice-variation={slice.variation}
        >
          <div className="mx-ato w-full max-w-6xl grid grid-cols-1 md:grid-cols-2 place-items-center text-center">
            <div className="flex gap-5 flex-col items-center">
              <PrismicRichText
                field={slice.primary.heading}
                components={components}
              />
              <PrismicRichText
                field={slice.primary.body}
                components={components}
              />

              <Button
                field={slice.primary.button_link}
                className="block w-fit bg-cyan-700 hover:bg-cyan-800 px-12 text-white rounded-full"
              >
                {slice.primary.button_text}
              </Button>
            </div>

            <PrismicNextImage
              field={slice.primary.image}
              className="drop-shadow-xl max-w-4xl w-full"
            />
          </div>
        </section>
      )}
    </>
  );
};

export default Hero;
