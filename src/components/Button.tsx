import { PrismicNextLink, PrismicNextLinkProps } from "@prismicio/next";
import clsx from "clsx";

export default function Button({ className, ...restProps}: PrismicNextLinkProps) {
  return(
    <PrismicNextLink className={clsx("block w-fit bg-cyan-700 hover:bg-cyan-800 px-12 py-4 text-white rounded-full",className)} {...restProps} />

  )
}