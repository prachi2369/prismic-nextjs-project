import { createClient } from "@/prismicio";
import { PrismicNextLink } from "@prismicio/next";
import Link from "next/link";
import Icon from "./Logo";

export default async function Header() {
  const client = createClient();
  const settings = await client.getSingle("settings");
  return (
    <header className="py-4 md:py-6 lg:py-8 px-10">
      <div className="flex gap-4 items-center sm:flex-row flex-col justify-between">
        <Link href="/"><Icon /></Link>

        <nav>
          <ul className="flex">
            {settings.data.navigation.map(({ link, label }) => (
              <li key={label} className="p-3">
                <PrismicNextLink field={link}>{label}</PrismicNextLink>
              </li>

            ))}
          </ul>
        </nav>
      </div>
    </header>
  );
}
