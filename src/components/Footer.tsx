import { createClient } from "@/prismicio";
import { PrismicNextLink } from "@prismicio/next";
import Link from "next/link";
import Icon from "./Logo";

export default async function Footer() {
  const client = createClient();
  const settings = await client.getSingle("settings");
  return (
    <footer className="p-4 md:p-6 lg:p-8">
      <div className="flex sm:flex-row flex-col justify-between text-center gap-6">
        <Link href="/"><Icon /></Link>
        <p>@{new Date().getFullYear()}</p>
        <nav>
          <ul className="flex">
            {settings.data.navigation.map(({ link, label }) => (
              <li key={label}>
                <PrismicNextLink field={link} className="p-3">{label}</PrismicNextLink>
              </li>

            ))}
          </ul>
        </nav>
      </div>
    </footer>
  );
}
